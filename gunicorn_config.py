import os

bind = os.getenv('GUNICORN_BIND', '0.0.0.0:8088')
limit_request_field_size = int(os.getenv('GUNICORN_LIMIT_REQUEST_FIELD_SIZE', '8190'))
limit_request_line = int(os.getenv('GUNICORN_LIMIT_REQUEST_LINE', '4094'))
threads = int(os.getenv('GUNICORN_THREADS', '4'))
timeout = int(os.getenv('GUNICORN_TIMEOUT', '120'))
workers = int(os.getenv('GUNICORN_WORKERS', '4'))
max_requests = int(os.getenv('GUNICORN_MAX_REQUESTS', '1000'))
worker_class = os.getenv('GUNICORN_WORKER_CLASS', 'gevent')
