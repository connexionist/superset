FROM python:3.8.6-buster

ARG SUPERSET_VERSION=0.37.2
ARG SUPERSET_HOME=/home/superset    
ARG TINI_VERSION=v0.19.0

ENV GUNICORN_BIND=0.0.0.0:8088 \
    GUNICORN_LIMIT_REQUEST_FIELD_SIZE=8190 \
    GUNICORN_LIMIT_REQUEST_LINE=4094 \
    GUNICORN_THREADS=4 \
    GUNICORN_TIMEOUT=120 \
    GUNICORN_WORKERS=4 \
    GUNICORN_WORKER_CLASS=gevent \
    LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    PYTHONPATH=$SUPERSET_HOME:$PYTHONPATH    

RUN useradd -U -m superset \
    && apt-get update -y \
    && apt-get install -y \
    build-essential \
    curl \    
    default-libmysqlclient-dev \
    freetds-bin \
    freetds-dev \
    libaio1 \
    libffi-dev \
    libldap2-dev \
    libpq-dev \
    libsasl2-2 \
    libsasl2-dev \
    libsasl2-modules-gssapi-mit \
    libssl1.1 \
    && apt-get clean

ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

COPY requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt \
    && pip install apache-superset==$SUPERSET_VERSION

WORKDIR $SUPERSET_HOME
USER superset
COPY gunicorn_config.py ./

EXPOSE 8088
CMD ["gunicorn", "-c", "gunicorn_config.py", "superset.app:create_app()"]
HEALTHCHECK CMD ["curl", "-f", "http://localhost:8088/health"]
